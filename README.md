# scooters_rental READ ME file

Functional Data Programming project - Scooter Rental
By Damien LEBRET, Vincent DAÏRIEN and Léo MERRAN

## I) How to run the project :

*Terminal 1 : launch Kafka*

bin/kafka-server-start.sh config/server.properties

*Terminal 2 : launch zookeeper*

bin/zookeeper-server-start.sh config/zookeeper.properties

*Terminal 3 : create 2 topics*

bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1
--topic "my_topic_1"

bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1
--topic "my_topic_2"

*Terminal 3 : launch producer*

bin/kafka-console-producer.sh --broker-list localhost:2181 --topic "my_topic_1"

*Terminal 4 : launch consumer*

bin/kafka-console-consumer.sh --bootstrap-server localhost:2181 --topic "my_topic-1"

## II) General description :

To this project, we decided to simulate a scooters (trotinettes) rental. The consumer
can with our program obtain informations about the actual situation of rentals. For example,
he can see the top 5 of the city which provide the more scooters, see which scooters are
available in each city, see how many scooters are available in each city, know which
scooters are breakdown by city, know the number of breakdown scooters by city and finally
know the operational scooters by city.

*In the repository, you can see the actual use case diagram of our project.*


## III) Architecture :

Kafka uses ZooKeeper to manage the cluster. ZooKeeper is used to coordinate the
brokers/cluster configuration. We used Spark in our project with Kafka and created a JSON